package com.itv.supermarket;

import com.itv.supermarket.repository.impl.InMemoryOfferRepo;
import com.itv.supermarket.repository.impl.InMemoryProductRepo;
import com.itv.supermarket.services.CheckOutService;
import com.itv.supermarket.services.OffersService;
import com.itv.supermarket.services.ProductService;
import com.itv.supermarket.services.impl.CheckOutServiceImpl;
import com.itv.supermarket.services.impl.OffersServiceImpl;
import com.itv.supermarket.services.impl.ProductServiceImpl;

public class Main {


    public static void main(String[] args) {
        ProductService productService = new ProductServiceImpl(new InMemoryProductRepo());
        OffersService offersService = new OffersServiceImpl(new InMemoryOfferRepo());
        CheckOutService checkOutService = new CheckOutServiceImpl(productService, offersService);
    }
}



