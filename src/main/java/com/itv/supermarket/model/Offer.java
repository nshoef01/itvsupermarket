package com.itv.supermarket.model;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

public interface Offer {

    UUID getOfferId();

    List<UUID> getProducts();

    String getOfferName();

    boolean haveReduction(List<LineItem> items);

    BigDecimal getReduction(List<LineItem> items);

}
