package com.itv.supermarket.model;

import java.math.BigDecimal;
import java.util.UUID;

public interface Product {

    UUID getProductId();

    String getName();

    void setName(String name);

    BigDecimal getPrice();

    void SetPrice(BigDecimal price);
}
