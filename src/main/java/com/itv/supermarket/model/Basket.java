package com.itv.supermarket.model;


import java.util.Map;
import java.util.UUID;

public interface Basket {

    UUID getBasketId();

    void addItem(UUID productId);

    void addItems(UUID productId, int amount);

    void removeAll(UUID productId);

    void removeItems(UUID productId, int amount);

    Map<UUID, Integer> getItems();

}
