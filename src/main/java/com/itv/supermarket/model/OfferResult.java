package com.itv.supermarket.model;

import java.math.BigDecimal;

public class OfferResult {
    private final Offer offer;
    private final BigDecimal totalDiscount;

    public OfferResult(Offer offer, BigDecimal totalDiscount) {
        this.offer = offer;
        this.totalDiscount = totalDiscount;
    }

    public Offer getOffer() {
        return offer;
    }

    public BigDecimal getTotalDiscount() {
        return totalDiscount;
    }
}
