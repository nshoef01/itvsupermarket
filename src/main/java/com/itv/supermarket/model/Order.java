package com.itv.supermarket.model;

import java.math.BigDecimal;
import java.util.List;

public class Order {
    private final List<LineItem> lineItems;
    private final List<OfferResult> offerResults;
    private final BigDecimal totalPrice;


    public Order(List<LineItem> lineItems, List<OfferResult> offerResults, BigDecimal totalPrice) {
        this.lineItems = lineItems;
        this.offerResults = offerResults;
        this.totalPrice = totalPrice;
    }

    public List<LineItem> getLineItems() {
        return lineItems;
    }

    public List<OfferResult> getOfferResults() {
        return offerResults;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }
}


