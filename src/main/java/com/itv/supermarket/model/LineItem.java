package com.itv.supermarket.model;


import java.math.BigDecimal;
import java.util.UUID;

public class LineItem {
    private final UUID productId;
    private final int amount;
    private final BigDecimal price;
    private final BigDecimal totalPrice;

    public LineItem(UUID productId, int amount, BigDecimal price) {
        this.productId = productId;
        this.amount = amount;
        this.price = price;
        totalPrice = price.multiply(BigDecimal.valueOf(amount));
    }

    public UUID getProductId() {
        return productId;
    }

    public int getAmount() {
        return amount;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }
}
