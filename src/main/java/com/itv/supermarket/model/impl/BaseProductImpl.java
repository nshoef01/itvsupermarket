package com.itv.supermarket.model.impl;

import com.itv.supermarket.model.Product;

import java.math.BigDecimal;
import java.util.UUID;
import java.util.function.Supplier;

public class BaseProductImpl implements Product {
    private UUID productId;
    private String name;
    private BigDecimal price;

    public BaseProductImpl(UUID productId, String name, BigDecimal price) {
        this.productId = productId;
        this.name = name;
        this.price = price;
    }

    @Override
    public UUID getProductId() {
        return productId;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public BigDecimal getPrice() {
        return price;
    }

    @Override
    public void SetPrice(BigDecimal price) {
        this.price = price;
    }


    public class Builder implements Supplier<Product> {
        private UUID productId;
        private String name;
        private BigDecimal price;


        public Builder withProductId(UUID productId) {
            this.productId = productId;
            return this;
        }

        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        public Builder withPrice(BigDecimal price) {
            this.price = price;
            return this;
        }

        public Builder fromProduct(Product product) {
            this.productId = product.getProductId();
            this.name = product.getName();
            this.price = product.getPrice();
            return this;
        }


        @Override
        public Product get() {
            if(productId == null) throw new NullPointerException("ProductId is null");
            if(name == null) throw new NullPointerException("Name is null");
            if(price == null) throw new NullPointerException("Price is null");
            return new BaseProductImpl(productId, name, price);
        }
    }
}
