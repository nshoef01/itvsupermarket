package com.itv.supermarket.model.impl;

import com.itv.supermarket.model.Basket;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class BasketImpl implements Basket {
    private UUID basketId;
    private Map<UUID, Integer> items;


    public BasketImpl() {
        basketId = UUID.randomUUID();
        items = new HashMap<>();
    }


    @Override
    public UUID getBasketId() {
        return basketId;
    }

    @Override
    public void addItem(UUID productId) {
        addItems(productId, 1);
    }

    //
    @Override
    public void addItems(UUID productId, int amount) {
        if(amount<1) throw new IllegalArgumentException("Amount must be a positive number");
        Integer value = Integer.valueOf(amount);
        if(items.containsKey(productId)) {
            value += items.get(productId);
        }
        items.put(productId, value);
    }

    @Override
    public void removeAll(UUID productId) {
        if(!items.containsKey(productId)) throw new IllegalArgumentException("Item not in basket");
        items.remove(productId);
    }

    @Override
    public void removeItems(UUID productId, int amount) {
        if(!items.containsKey(productId)) throw new IllegalArgumentException("Item not in basket");
        int currentAmount = items.get(productId);
        if(currentAmount < amount) throw new IllegalArgumentException("Can not remove more than what in the basket");

        int newAmount = currentAmount -amount;
        if(newAmount == 0) items.remove(productId);
        else items.put(productId, newAmount);
    }

    @Override
    public Map<UUID, Integer> getItems() {
        return items;
    }
}
