package com.itv.supermarket.model.impl;

import com.itv.supermarket.model.LineItem;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class BuyNForLessOffer extends BaseOfferImpl {
    private int numberOfItems;
    private BigDecimal pricePerNItems;


    public BuyNForLessOffer(String name, UUID product, int numberOfItems, BigDecimal pricePerNItems) {
        super(name);
        products.add(product);
        if(numberOfItems < 1) throw new IllegalArgumentException("Number of items must be at least 1");
        if(pricePerNItems.doubleValue() <= 0) throw new IllegalArgumentException("Price must be more then 1");
        this.numberOfItems = numberOfItems;
        this.pricePerNItems = pricePerNItems;
    }

    @Override
    public boolean haveReduction(List<LineItem> items) {
        return items.stream()
                .filter(lineItem -> getProducts().contains(lineItem.getProductId()))
                .filter(lineItem -> lineItem.getAmount() >= numberOfItems)
                .count() > 0;
    }

    @Override
    public BigDecimal getReduction(List<LineItem> items) {
        List<LineItem> eligibleItem = items.stream()
                .filter(item -> products.contains(item.getProductId()))
                .collect(Collectors.toList());

        if(eligibleItem.isEmpty()) return BigDecimal.ZERO;

        // since we only add a single product in this offer, we expect the reducer not to add anything as there is one one item
        return eligibleItem.stream().map(item -> calculateDiscount(item)).reduce((x, y) -> x.add(y)).get();
    }

    private BigDecimal calculateDiscount(LineItem lineItem) {
        int eligibleNumber = lineItem.getAmount() / numberOfItems;
        BigDecimal fullPrice = lineItem.getPrice().multiply(BigDecimal.valueOf(numberOfItems));
        return fullPrice.subtract(pricePerNItems).multiply(BigDecimal.valueOf(eligibleNumber));
    }
}
