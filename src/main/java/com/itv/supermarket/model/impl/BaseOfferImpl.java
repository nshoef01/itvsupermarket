package com.itv.supermarket.model.impl;

import com.itv.supermarket.model.Offer;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public abstract class BaseOfferImpl implements Offer {
    protected final UUID offerId;
    protected final List<UUID> products;
    protected String name;


    public BaseOfferImpl(String name) {
        offerId = UUID.randomUUID();
        products = new ArrayList<>();
        this.name = name;
    }

    @Override
    public UUID getOfferId() { return offerId; }

    @Override
    public List<UUID> getProducts() {
        return products;
    }

    public String getOfferName() {
        return name;
    }
}
