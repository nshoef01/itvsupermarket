package com.itv.supermarket.repository.impl;

import com.itv.supermarket.model.Product;
import com.itv.supermarket.repository.ProductRepo;

import java.util.*;

public class InMemoryProductRepo implements ProductRepo {
    private final Map<UUID, Product> products;


    public InMemoryProductRepo() {
        products = new HashMap<>();
    }

    @Override
    public void createProduct(Product product) {
        if(products.get(product.getProductId()) != null) throw new IllegalArgumentException("Product already exist");

        products.put(product.getProductId(), product);
    }

    @Override
    public void updateProduct(Product product) {
        if(products.get(product.getProductId()) == null) throw new IllegalArgumentException("Product not exist");
        products.put(product.getProductId(), product);
    }

    @Override
    public Product getProduct(UUID productId) {
        return products.get(productId);
    }

    @Override
    public Collection<Product> getProducts() {
        return products.values();
    }
}
