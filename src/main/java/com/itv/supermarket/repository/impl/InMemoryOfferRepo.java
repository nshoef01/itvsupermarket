package com.itv.supermarket.repository.impl;

import com.itv.supermarket.model.Offer;
import com.itv.supermarket.repository.OfferRepo;

import java.util.*;

public class InMemoryOfferRepo implements OfferRepo {
    private final Map<UUID, Offer> offers;


    public InMemoryOfferRepo() {
        offers = new HashMap<>();
    }
    @Override
    public void addOffer(Offer offer) {
        if(offers.get(offer.getOfferId()) != null) throw new IllegalArgumentException("Offer already exist");
        offers.put(offer.getOfferId(), offer);
    }

    @Override
    public void removeOffer(UUID offerId) {
        if(offers.get(offerId) == null) throw new IllegalArgumentException("Offer not exist");
        offers.remove(offerId);
    }

    @Override
    public Offer getOffer(UUID offerId) {
        return offers.get(offerId);
    }

    @Override
    public Collection<Offer> getOffers() {
        return offers.values();
    }
}
