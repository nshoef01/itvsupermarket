package com.itv.supermarket.repository;

import com.itv.supermarket.model.Offer;

import java.util.Collection;
import java.util.UUID;

public interface OfferRepo {

    void addOffer(Offer offer);

    void removeOffer(UUID offerId);

    Offer getOffer(UUID offerId);

    Collection<Offer> getOffers();
}
