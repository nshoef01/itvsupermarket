package com.itv.supermarket.repository;

import com.itv.supermarket.model.Product;

import java.util.Collection;
import java.util.UUID;


public interface ProductRepo {

    void createProduct(Product product);

    void updateProduct(Product product);

    Product getProduct(UUID productId);

    Collection<Product> getProducts();

}
