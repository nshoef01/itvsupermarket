package com.itv.supermarket.services;

import com.itv.supermarket.model.Product;

import java.util.Collection;
import java.util.UUID;

public interface ProductService {

    void createProduct(Product product);

    void updateProduct(Product product);

    Product getProduct(UUID productId);

    Collection<Product> getAllProducts();

}


