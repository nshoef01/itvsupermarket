package com.itv.supermarket.services.impl;

import com.itv.supermarket.model.Offer;
import com.itv.supermarket.repository.OfferRepo;
import com.itv.supermarket.services.OffersService;

import java.util.Collection;
import java.util.UUID;

public class OffersServiceImpl implements OffersService {
    private OfferRepo offerRepo;

    public OffersServiceImpl(OfferRepo offerRepo) {
        this.offerRepo = offerRepo;
    }

    @Override
    public void addOffer(Offer offer) {
        offerRepo.addOffer(offer);
    }

    @Override
    public void removeOffer(UUID offerId) {
        offerRepo.removeOffer(offerId);
    }

    @Override
    public Offer getOffer(UUID offerId) {
        return offerRepo.getOffer(offerId);
    }

    @Override
    public Collection<Offer> getAllOffers() {
        return offerRepo.getOffers();
    }
}
