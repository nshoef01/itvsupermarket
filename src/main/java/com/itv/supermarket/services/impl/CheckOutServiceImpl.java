package com.itv.supermarket.services.impl;

import com.itv.supermarket.model.*;
import com.itv.supermarket.services.CheckOutService;
import com.itv.supermarket.services.OffersService;
import com.itv.supermarket.services.ProductService;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

public class CheckOutServiceImpl implements CheckOutService {
    private ProductService productService;
    private OffersService offersService;


    public CheckOutServiceImpl(ProductService productService, OffersService offersService) {
        this.productService = productService;
        this.offersService = offersService;
    }

    @Override
    public Order checkout(Basket basket) {
        List<LineItem> lineItems = generateLineItems(basket.getItems());
        Collection<Offer> offers = offersService.getAllOffers();
        List<OfferResult> offerResults = offers.stream()
                .filter(offer -> offer.haveReduction(lineItems))
                .map(offer -> new OfferResult(offer, offer.getReduction(lineItems)))
                .collect(Collectors.toList());

        BigDecimal totalPrice = getTotalPrice(lineItems, offerResults);
        return new Order(lineItems, offerResults, totalPrice);
    }

    private List<LineItem> generateLineItems(Map<UUID, Integer> items) {
        return items.keySet().stream()
                .map(key -> new LineItem(key, items.get(key), productService.getProduct(key).getPrice()))
                .collect(Collectors.toList());
    }


    private BigDecimal getTotalPrice(List<LineItem> lineItems, List<OfferResult> offerResults) {
        BigDecimal beforeReduction = lineItems.stream()
                .map(lineItem -> lineItem.getTotalPrice())
                .reduce((x,y) -> x.add(y))
                .get();

        BigDecimal totalReduction = offerResults.stream()
                .map(offerResult -> offerResult.getTotalDiscount())
                .reduce((x,y) -> x.add(y))
                .get();

        return beforeReduction.subtract(totalReduction);
    }
}
