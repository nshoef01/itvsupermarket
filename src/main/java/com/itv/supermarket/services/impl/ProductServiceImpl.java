package com.itv.supermarket.services.impl;

import com.itv.supermarket.model.Product;
import com.itv.supermarket.repository.ProductRepo;
import com.itv.supermarket.services.ProductService;

import java.util.Collection;
import java.util.UUID;

public class ProductServiceImpl implements ProductService {
    private ProductRepo productRepo;


    public ProductServiceImpl(ProductRepo productRepo) {
        this.productRepo = productRepo;
    }

    @Override
    public void createProduct(Product product) {
        productRepo.createProduct(product);
    }

    @Override
    public void updateProduct(Product product) {
        productRepo.updateProduct(product);
    }

    @Override
    public Product getProduct(UUID productId) {
        return productRepo.getProduct(productId);
    }

    @Override
    public Collection<Product> getAllProducts() {
        return productRepo.getProducts();
    }
}
