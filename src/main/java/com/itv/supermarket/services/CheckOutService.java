package com.itv.supermarket.services;

import com.itv.supermarket.model.Basket;
import com.itv.supermarket.model.Order;

public interface CheckOutService {


    Order checkout(Basket basket);
}
