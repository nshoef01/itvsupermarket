package com.itv.supermarket.services;

import com.itv.supermarket.model.Offer;

import java.util.Collection;
import java.util.UUID;

public interface OffersService {

    void addOffer(Offer offer);

    void removeOffer(UUID offerId);

    Offer getOffer(UUID offerId);

    Collection<Offer> getAllOffers();
}
