package com.itv.supermarket.model.impl

import com.itv.supermarket.model.Product
import spock.lang.Specification

class BaseProductImplUnitSpec extends Specification {


    def "Should create a product with all values"() {
        given:
        def id = UUID.randomUUID()
        def name = "Coke"
        def price = BigDecimal.valueOf(1.2)

        when:
        Product product = new BaseProductImpl.Builder()
                .withProductId(id)
                .withName(name)
                .withPrice(price)
                .get()
        then:
        product.getProductId() == id
        product.getName() == name
        product.getPrice() == price
    }

    def "Should not allow building of object with missing mandatory fields"() {
        given:
        def id = UUID.randomUUID()
        def name = "Coke"

        when:
        Product product = new BaseProductImpl.Builder()
                .withProductId(id)
                .withName(name)
                .get()
        then:
        NullPointerException ex = thrown()
        ex.message == "Price is null"
    }

    def "Should allow updating fields" () {
        given:
        def product = new BaseProductImpl(UUID.randomUUID(), "Pepsi", BigDecimal.valueOf(1.1))
        def newName= "Pepsi Cola"
        when:
        product.setName(newName)
        then:
        product.getName() == newName
        and:
        when:
        def newPrice = BigDecimal.valueOf(1.2)
        def updatedProduct = new BaseProductImpl.Builder().fromProduct(product).withPrice(newPrice).get()
        then:
        updatedProduct.getPrice() == newPrice
        updatedProduct.getName() == newName
        updatedProduct.getProductId() == product.getProductId()
    }
}
