package com.itv.supermarket.model.impl

import spock.lang.Specification

class BasketImplUnitSpec extends Specification {
    private BasketImpl basket


    def setup() {
        basket = new BasketImpl()
    }

    def "Should allow adding item to the basket"() {
        given:
        def product1 = UUID.randomUUID()
        def product2 = UUID.randomUUID()
        when:
        basket.addItem(product1)
        basket.addItem(product2)
        then:
        basket.getItems().get(product1) == 1
        basket.getItems().get(product2) == 1
        and:
        when:
        basket.addItem(product1)
        then:
        basket.getItems().get(product1) == 2
        basket.getItems().get(product2) == 1
    }

    def "Should allow adding n amount of an item to the basket"() {
        given:
        def product1 = UUID.randomUUID()
        def product2 = UUID.randomUUID()
        when:
        basket.addItems(product1, 3)
        basket.addItems(product2, 1)
        then:
        basket.getItems().get(product1) == 3
        basket.getItems().get(product2) == 1
        and:
        when:
        basket.addItems(product1, 2)
        then:
        basket.getItems().get(product1) == 5
        basket.getItems().get(product2) == 1
    }

    def "should throw exception when trying to add a negative amount of items"() {
        given:
        def product1 = UUID.randomUUID()
        when:
        basket.addItems(product1, -3)
        then:
        IllegalArgumentException ex = thrown()
        ex.message == "Amount must be a positive number"
    }

    def "Should allow removing n amount of item from the basket"() {
        given:
        def product1 = UUID.randomUUID()
        def product2 = UUID.randomUUID()
        basket.addItems(product1, 3)
        basket.addItems(product2, 4)
        when:
        basket.removeItems(product1, 3)
        basket.removeItems(product2, 2)
        then:
        basket.getItems().get(product1) == null
        basket.getItems().get(product2) == 2
    }

    def "Should throw exception when trying to remove higher amount then what in the basket"() {
        given:
        def product1 = UUID.randomUUID()
        basket.addItems(product1, 3)
        when:
        basket.removeItems(product1, 4)
        then:
        IllegalArgumentException ex = thrown()
        ex.message == "Can not remove more than what in the basket"
    }

    def "Should throw exception when trying to remove an item which is not in the basket" () {
        given:
        def product1 = UUID.randomUUID()
        basket.addItems(product1, 3)
        when:
        basket.removeItems(product1, 4)
        then:
        IllegalArgumentException ex = thrown()
        ex.message == "Can not remove more than what in the basket"
    }

    def "Should allow removing all items from the basket" () {
        given:
        def product1 = UUID.randomUUID()
        basket.addItems(product1, 3)
        when:
        basket.removeAll(product1)
        then:
        basket.getItems().get(product1) == null
    }
}
