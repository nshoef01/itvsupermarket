package com.itv.supermarket.model.impl

import com.itv.supermarket.model.LineItem
import spock.lang.Specification

class BuyNForLessOfferUnitSpec extends Specification {


    def "Should give the correct reduction when items are eligible for the offer"() {
        given:
        def productId = UUID.randomUUID()
        def lineItem = new LineItem(productId, 3, BigDecimal.valueOf(0.4))
        BuyNForLessOffer offer = new BuyNForLessOffer("Buy 2 for 1", productId, 2, BigDecimal.valueOf(0.4))
        when:
        def result = offer.getReduction([lineItem])
        then:
        result == BigDecimal.valueOf(0.4)
        and:
        offer.haveReduction([lineItem]) == true
    }

    def "Should not give reduction when items are not eligible for the offer" () {
        given:
        def productId = UUID.randomUUID()
        def lineItem = new LineItem(UUID.randomUUID(), 2, BigDecimal.valueOf(0.4))
        BuyNForLessOffer offer = new BuyNForLessOffer("Buy 2 for 1", productId, 2, BigDecimal.valueOf(0.4))
        when:
        def result = offer.getReduction([lineItem])
        then:
        result == BigDecimal.ZERO
        and:
        offer.haveReduction([lineItem]) == false

    }

    def "Should throw exception if numberOfItem is not positive"() {
        when:
        def offer = new BuyNForLessOffer("Non sense offer", UUID.randomUUID(), -3, 3)
        then:
        IllegalArgumentException ex = thrown()
        ex.message == "Number of items must be at least 1"
    }

    def "Should throw exception if price is not positive"() {
        when:
        def offer = new BuyNForLessOffer("Non sense offer", UUID.randomUUID(), 2, -1)
        then:
        IllegalArgumentException ex = thrown()
        then:
        ex.message == "Price must be more then 1"
    }
}
