package com.itv.supermarket

import com.itv.supermarket.model.impl.BaseProductImpl
import com.itv.supermarket.model.impl.BasketImpl
import com.itv.supermarket.model.impl.BuyNForLessOffer
import com.itv.supermarket.repository.impl.InMemoryOfferRepo
import com.itv.supermarket.repository.impl.InMemoryProductRepo
import com.itv.supermarket.services.CheckOutService
import com.itv.supermarket.services.OffersService
import com.itv.supermarket.services.ProductService
import com.itv.supermarket.services.impl.CheckOutServiceImpl
import com.itv.supermarket.services.impl.OffersServiceImpl
import com.itv.supermarket.services.impl.ProductServiceImpl
import spock.lang.Specification

class IntegrationTest extends Specification {
    private ProductService productService
    private OffersService offersService
    private CheckOutService checkOutService

    final def A_ID = UUID.randomUUID()
    final def B_ID = UUID.randomUUID()
    final def C_ID = UUID.randomUUID()
    final def D_ID = UUID.randomUUID()
    final def E_ID = UUID.randomUUID()

    def offer1
    def offer2
    def offer3


    def setup() {
        productService  = new ProductServiceImpl(new InMemoryProductRepo())
        offersService = new OffersServiceImpl(new InMemoryOfferRepo())
        checkOutService = new CheckOutServiceImpl(productService, offersService)

        populateProducts()
        populateOffers()
    }

    def populateProducts() {
        [
                new BaseProductImpl.Builder()
                        .withProductId(A_ID)
                        .withName("A")
                        .withPrice(BigDecimal.valueOf(1.2))
                        .get(),

                new BaseProductImpl.Builder()
                        .withProductId(B_ID)
                        .withName("B")
                        .withPrice(BigDecimal.valueOf(0.4))
                        .get(),

                new BaseProductImpl.Builder()
                        .withProductId(C_ID)
                        .withName("C")
                        .withPrice(BigDecimal.valueOf(3.2))
                        .get(),

                new BaseProductImpl.Builder()
                        .withProductId(D_ID)
                        .withName("D")
                        .withPrice(BigDecimal.valueOf(1.35))
                        .get(),

                new BaseProductImpl.Builder()
                        .withProductId(E_ID)
                        .withName("E")
                        .withPrice(BigDecimal.valueOf(0.99))
                        .get(),

        ].forEach{ p -> productService.createProduct(p) }
    }

    def populateOffers() {
        offer1 = new BuyNForLessOffer("Buy 3 B for 1 GBP", B_ID, 3, BigDecimal.ONE)
        offer2 = new BuyNForLessOffer("Buy 5 E pay for 4", E_ID, 5, BigDecimal.valueOf(3.96))
        offersService.addOffer(offer1)
        offersService.addOffer(offer2)
    }


    def "Should perform correct checkout with the given items"() {

        given: "I create a basket"
        def basket = new BasketImpl()

        and: "add items to it in random order"

        basket.addItems(A_ID, 3)
        basket.addItems(D_ID, 2)
        basket.addItems(B_ID, 1)
        basket.addItem(E_ID)
        basket.addItem(E_ID)
        basket.addItem(E_ID)
        basket.addItems(D_ID, 2)
        basket.addItems(E_ID, 3)
        basket.addItem(C_ID)

        and: "I perform an update of prices"
        productService.updateProduct(new BaseProductImpl.Builder()
                .fromProduct(productService.getProduct(A_ID))
                .withPrice(BigDecimal.ONE)
                .get())

        and: "I add some more items"
        basket.addItem(B_ID)

        and: "I perform some update on the offers"
        offer3 = new BuyNForLessOffer("Buy 1 for half price", C_ID, 1, 1.6)
        offersService.addOffer(offer3)
        offersService.removeOffer(offer2.getOfferId())

        when: "I perform a checkout on my basket"
        def order = checkOutService.checkout(basket)

        then: "I get and order with the up-to-date prices and offers"
        def lineItems = order.getLineItems()
        lineItems.size() == 5
        lineItems.sort{x,y -> x.price <=> y.price}
        def lineItem1 = lineItems.get(0)
        def lineItem2 = lineItems.get(1)
        def lineItem3 = lineItems.get(2)
        def lineItem4 = lineItems.get(3)
        def lineItem5 = lineItems.get(4)

        lineItem1.productId == B_ID
        lineItem1.price ==  0.4
        lineItem1.totalPrice == 0.8
        lineItem1.amount == 2

        lineItem2.productId == E_ID
        lineItem2.price ==  0.99
        lineItem2.totalPrice == 5.94
        lineItem2.amount == 6

        lineItem3.productId == A_ID
        lineItem3.price ==  1
        lineItem3.totalPrice  == 3
        lineItem3.amount == 3

        lineItem4.productId == D_ID
        lineItem4.price ==  1.35
        lineItem4.totalPrice == 5.4
        lineItem4.amount == 4

        lineItem5.productId == C_ID
        lineItem5.price ==  3.2
        lineItem5.totalPrice == 3.2
        lineItem5.amount == 1

        def offerResults = order.offerResults
        offerResults.size() == 1
        offerResults.offer.contains(offer3)
        offerResults.totalDiscount.contains(1.6)

        order.totalPrice == BigDecimal.valueOf(16.74)
    }
}
