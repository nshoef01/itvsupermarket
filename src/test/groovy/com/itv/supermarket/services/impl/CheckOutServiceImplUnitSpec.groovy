package com.itv.supermarket.services.impl

import com.itv.supermarket.model.impl.BaseProductImpl
import com.itv.supermarket.model.impl.BasketImpl
import com.itv.supermarket.model.impl.BuyNForLessOffer
import com.itv.supermarket.repository.impl.InMemoryOfferRepo
import com.itv.supermarket.repository.impl.InMemoryProductRepo
import com.itv.supermarket.services.OffersService
import com.itv.supermarket.services.ProductService
import spock.lang.Specification

class CheckOutServiceImplUnitSpec extends Specification {
    private CheckOutServiceImpl checkOutService
    private ProductService productService
    private OffersService offersService

    final def COKE_ID = UUID.randomUUID()
    final def MILK_ID = UUID.randomUUID()
    final def CHICKEN_ID = UUID.randomUUID()
    final def EGGS_ID = UUID.randomUUID()
    final def CHOCOLATE_ID = UUID.randomUUID()

    def offer1
    def offer2


    def setup() {
        productService = new ProductServiceImpl(new InMemoryProductRepo())
        offersService = new OffersServiceImpl(new InMemoryOfferRepo())
        checkOutService = new CheckOutServiceImpl(productService, offersService)

        populateProducts()
        populateOffers()
    }

    def populateProducts() {
        [
                new BaseProductImpl.Builder()
                        .withProductId(COKE_ID)
                        .withName("Coke")
                        .withPrice(BigDecimal.valueOf(1.2))
                        .get(),

                new BaseProductImpl.Builder()
                        .withProductId(MILK_ID)
                        .withName("Milk")
                        .withPrice(BigDecimal.valueOf(0.4))
                        .get(),

                new BaseProductImpl.Builder()
                        .withProductId(CHICKEN_ID)
                        .withName("Chicken")
                        .withPrice(BigDecimal.valueOf(3.2))
                        .get(),

                new BaseProductImpl.Builder()
                        .withProductId(EGGS_ID)
                        .withName("Eggs")
                        .withPrice(BigDecimal.valueOf(1.35))
                        .get(),

                new BaseProductImpl.Builder()
                        .withProductId(CHOCOLATE_ID)
                        .withName("Chocolate")
                        .withPrice(BigDecimal.valueOf(0.99))
                        .get(),

        ].forEach{ p -> productService.createProduct(p) }
    }

    def populateOffers() {
        offer1 = new BuyNForLessOffer("Buy 3 milk for 1 GBP", MILK_ID, 3, BigDecimal.ONE)
        offer2 = new BuyNForLessOffer("Buy 5 chocolates pay for 4", CHOCOLATE_ID, 5, BigDecimal.valueOf(3.96))
        offersService.addOffer(offer1)
        offersService.addOffer(offer2)
    }


    def "Should perform a correct checkout on a given basket" () {

        given: "I created a basket"
        def basket = new BasketImpl()

        and: "I added items to it"

        basket.addItems(COKE_ID, 3) // 3.6
        basket.addItems(MILK_ID, 1) // 0.4
        basket.addItems(EGGS_ID, 4) // 5.40
        basket.addItems(CHOCOLATE_ID, 6) // 5.94

        when: "I perform checkout"
        def order = checkOutService.checkout(basket)

        then: "I get and order with the correct prices"
        def lineItems = order.getLineItems()
        lineItems.size() == 4
        lineItems.productId.containsAll([COKE_ID, MILK_ID, EGGS_ID, CHOCOLATE_ID])
        lineItems.price.containsAll(1.2, 0.4, 1.35, 0.99)
        lineItems.totalPrice.containsAll(3.6, 0.4, 5.40, 5.94)
        lineItems.amount.containsAll(3, 1, 4, 6)

        order.offerResults.size() == 1
        def offerResult = order.offerResults.get(0)
        offerResult.getTotalDiscount() == 0.99
        offerResult.getOffer() == offer2
        order.totalPrice == BigDecimal.valueOf(14.35)
    }
}
