package com.itv.supermarket.services.impl

import com.itv.supermarket.model.impl.BaseProductImpl
import com.itv.supermarket.repository.impl.InMemoryProductRepo
import spock.lang.Specification

class ProductServiceImplUnitSpec extends Specification {


    def "Should allow creating new products in the repo"() {
        given:
        def productService = new ProductServiceImpl(new InMemoryProductRepo())
        def product = new BaseProductImpl(UUID.randomUUID(), "Milk", BigDecimal.valueOf(0.5))
        when:
        productService.createProduct(product)
        then:
        def result = productService.getProduct(product.getProductId())
        result == product
    }

    def "Should allow updating existing products" () {
        given:
        def productService = new ProductServiceImpl(new InMemoryProductRepo())
        def id = UUID.randomUUID()
        def name = "Milk"
        def price = BigDecimal.valueOf(0.5)
        def updatedPrice = BigDecimal.valueOf(0.7)

        def product = new BaseProductImpl(id, name, price)
        productService.createProduct(product)
        def update = new BaseProductImpl.Builder()
                .fromProduct(product)
                .withPrice(updatedPrice)
                .get()
        when:
        productService.updateProduct(update)
        then:
        def updated = productService.getProduct(product.getProductId())
        updated.getName() == name
        updated.getPrice() == updatedPrice
    }
}
