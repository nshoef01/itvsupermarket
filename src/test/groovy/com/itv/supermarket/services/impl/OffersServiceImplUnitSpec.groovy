package com.itv.supermarket.services.impl

import com.itv.supermarket.model.impl.BuyNForLessOffer
import com.itv.supermarket.repository.impl.InMemoryOfferRepo
import spock.lang.Specification

class OffersServiceImplUnitSpec extends Specification {

    def "Should allow adding new Offer to the repo"() {
        given:
        def offerService = new OffersServiceImpl(new InMemoryOfferRepo())
        def offer = new BuyNForLessOffer("Buy 1 get 1", UUID.randomUUID(), 2, 2.5)
        when:
        offerService.addOffer(offer)
        then:
        def savedOffer = offerService.getOffer(offer.getOfferId())
        savedOffer == offer
    }

    def "Should allow removing offers" () {
        given:
        def offerService = new OffersServiceImpl(new InMemoryOfferRepo())
        def offer = new BuyNForLessOffer("Buy 1 get 1", UUID.randomUUID(), 2, 2.5)
        offerService.addOffer(offer)
        when:
        offerService.removeOffer(offer.getOfferId())
        then:
        offerService.getOffer(offer.getOfferId()) == null
    }

    def "Should return all offers" () {
        given:
        def offerservice = new OffersServiceImpl(new InMemoryOfferRepo())

        def offer1 = new BuyNForLessOffer("Buy 4 get 1", UUID.randomUUID(), 2, 10)
        def offer2 = new BuyNForLessOffer("Buy 3 for special price", UUID.randomUUID(), 2, 3)
        def offer3 = new BuyNForLessOffer("1 for 25% off", UUID.randomUUID(), 1, 23)

        offerservice.addOffer(offer1)
        offerservice.addOffer(offer2)
        offerservice.addOffer(offer3)

        when:
        def result = offerservice.getAllOffers()
        then:
        result.size() == 3
        result.containsAll([offer1, offer2, offer3])
    }
}
