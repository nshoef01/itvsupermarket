package com.itv.supermarket.repository.impl

import com.itv.supermarket.model.impl.BuyNForLessOffer
import spock.lang.Specification

class InMemoryOfferRepoUnitSpec extends Specification {


    def "Should allow adding new Offer to the repo"() {
        given:
        def offerRepo = new InMemoryOfferRepo()
        def offer = new BuyNForLessOffer("Buy 1 get 1", UUID.randomUUID(), 2, 2.5)
        when:
        offerRepo.addOffer(offer)
        then:
        def savedOffer = offerRepo.getOffer(offer.getOfferId())
        savedOffer == offer
    }

    def "Should not allow adding new Offer to the repo if offer already exist"() {
        given:
        def offerRepo = new InMemoryOfferRepo()
        def offer = new BuyNForLessOffer("Buy 1 get 1", UUID.randomUUID(), 2, 2.5)
        offerRepo.addOffer(offer)
        when:
        offerRepo.addOffer(offer)
        then:
        IllegalArgumentException ex = thrown()
        ex.message == "Offer already exist"
    }

    def "Should allow removing offers" () {
        given:
        def offerRepo = new InMemoryOfferRepo()
        def offer = new BuyNForLessOffer("Buy 1 get 1", UUID.randomUUID(), 2, 2.5)
        offerRepo.addOffer(offer)
        when:
        offerRepo.removeOffer(offer.getOfferId())
        then:
        offerRepo.getOffer(offer.getOfferId()) == null
    }

    def "Should throw exception when when trying to remove non existing offer" () {
        given:
        def offerRepo = new InMemoryOfferRepo()
        when:
        offerRepo.removeOffer(UUID.randomUUID())
        then:
        IllegalArgumentException ex = thrown()
        ex.message == "Offer not exist"
    }

    def "Should return all offers" () {
        given:
        def offerRepo = new InMemoryOfferRepo()

        def offer1 = new BuyNForLessOffer("Buy 4 get 1", UUID.randomUUID(), 2, 10)
        def offer2 = new BuyNForLessOffer("Buy 3 for special price", UUID.randomUUID(), 2, 3)
        def offer3 = new BuyNForLessOffer("1 for 25% off", UUID.randomUUID(), 1, 23)

        offerRepo.addOffer(offer1)
        offerRepo.addOffer(offer2)
        offerRepo.addOffer(offer3)

        when:
        def result = offerRepo.getOffers()
        then:
        result.size() == 3
        result.containsAll([offer1, offer2, offer3])
    }
}
