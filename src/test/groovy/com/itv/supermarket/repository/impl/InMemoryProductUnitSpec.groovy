package com.itv.supermarket.repository.impl

import com.itv.supermarket.model.impl.BaseProductImpl
import spock.lang.Specification

class InMemoryProductUnitSpec extends Specification {
    private InMemoryProductRepo productRepo


    def setup() {
        productRepo = new InMemoryProductRepo()
    }

    def "Should allow creating new products in the repo"() {
        given:
        def product = new BaseProductImpl(UUID.randomUUID(), "Milk", BigDecimal.valueOf(0.5))
        when:
        productRepo.createProduct(product)
        then:
        def result = productRepo.getProduct(product.getProductId())
        result == product
    }

    def "Should not allow creating a product if it already exist" () {
        given:
        def product = new BaseProductImpl(UUID.randomUUID(), "Milk", BigDecimal.valueOf(0.5))
        productRepo.createProduct(product)
        when:
        productRepo.createProduct(product)
        then:
        IllegalArgumentException ex = thrown()
        ex.message == "Product already exist"
    }

    def "Should allow updating existing products" () {
        given:
        def id = UUID.randomUUID()
        def name = "Milk"
        def price = BigDecimal.valueOf(0.5)
        def updatedPrice = BigDecimal.valueOf(0.7)

        def product = new BaseProductImpl(id, name, price)
        productRepo.createProduct(product)
        def update = new BaseProductImpl.Builder()
                .fromProduct(product)
                .withPrice(updatedPrice)
                .get()
        when:
        productRepo.updateProduct(update)
        then:
        def updated = productRepo.getProduct(product.getProductId())
        updated.getName() == name
        updated.getPrice() == updatedPrice
    }

    def "Should throw exception when trying to update non existing product"() {
        given:
        def product = new BaseProductImpl(UUID.randomUUID(), "Bread", BigDecimal.valueOf(1))
        when:
        productRepo.updateProduct(product)
        then:
        IllegalArgumentException ex = thrown()
        ex.message == "Product not exist"
    }

    def "Should return null if the product of a given id does not exist"() {
        when:
        def product = productRepo.getProduct(UUID.randomUUID())
        then:
        product == null
    }
}
