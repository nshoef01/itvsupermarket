# ITVSuperMarket

This is an implementation of a supermarket checkout service.

There are 3 services:
1. Product service - take care of product creation/update/retrieving
2. OfferService - take care of adding/removing offers
3. Checkout service enable to perform a checkout on a basket of products

For the scope of the task, and for testing purposes, an In memory repositories been created to store data.
In real application this implementation should be replace with a real data storage.

The integration test is design to "prove" that the app meet the requirements of this task.

Since this is not a complete app, running the main class will not do anything, it is assumes that the missing layers (e.g api/server)
will use the services to perform tasks.

The app is written in java using gradle build tool and is using Spoc test framework.
In order to run it you will need to make sure you have java, gradle, maven, and groovy installed in your computer.


To run the tests you can load the source into your preferred ide.
You can also run them through gradle by running:

>./gradlew clean test

An html report of the test will be in build/reports/tests/test/index.html
